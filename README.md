# Marc Record Parser

The project houses php code which can parse, handle, and display Marc records with not very much user code.

## 0.0.2
Added support for corporate entities (110 and 710)

## 0.0.1
Basic functionality
