<?php
/**
 * PHP basic parsing of Marc Record
 * @author Jake Paris
 *
 * @link http://www.loc.gov/marc/bibliographic/
 */

class MarcParser {

	private $rawMarc;
	private $parsedMarc = false;
	private $subjects = false;
	private $title = false;
	private $year = false;
	private $publisher = false;
	private $summary = false;
	private $people = false;
	private $corporateEntities = false;

	function __construct( $rawMarc = null ) {
		if( ! is_null($rawMarc) ){
			$this->setRawMarc( $rawMarc );
		}
	}

	function setRawMarc( $marc) {
		$this->rawMarc = (string) $marc;
	}

	function getSummary(){
		if( ! $this->summary ) :
			$m = $this->parseMarcRecord();
			$this->summary = $this->getValuesFor('520');
		endif;
		return $this->summary;
	}

	function getPublicationYear(){
		if( ! $this->year ) :
			$m = $this->parseMarcRecord();
			$year = $this->getSubfieldValuesFor('264', 'c', true);
			if($year)
				$year = preg_replace("_[^0-9]*_", '', $year);
			$this->year = $year;
		endif;
		return $this->year;
	}
	function getPublisher(){
		if( ! $this->publisher ) :
			$m = $this->parseMarcRecord();
			$pub = $this->getSubfieldValuesFor('264', 'b', true);
			$this->publisher = $pub;
		endif;
		return $this->publisher;
	}

	function getTitle( $includeSubfields = true ){
		if( ! $this->title ) :
			$t = $this->getValuesFor('245');
			if( ! $t )
				return false;
			$m = $this->parseMarcRecord();
			if( isset($m['245']['subfields']) && $includeSubfields ){
				$t .= implode(' ', $un['subfields']['b'] );
			}
			$this->title = $t;
		endif;
		return $this->title;
	}

	function getSubjects(){
		if( ! $this->subjects ) :
			$m = $this->parseMarcRecord();
			$this->subjects = array_filter($m, function($key){
				return strpos($key, '65') === 0;
			}, ARRAY_FILTER_USE_KEY);
		endif;
		return $this->subjects;
	}

	function getSubjectsList($onlyForCode=false, $returnType='string', $sep="<br>"){
		$subjects = $this->getSubjects();
		$out = [];
		foreach($subjects as $code => $subjectCodeArray){
			if( $onlyForCode && $onlyForCode != $code)
				continue;
			foreach($subjectCodeArray as $subject) {
				$item = array($subject['value']);
				foreach($subject['subfields'] as $subfieldCode => $subfieldValues ){
					// have to strval(), otherwise integer of 0 is evaluated to
					// true and we get that fields's value
					switch( strval($subfieldCode) ){
						case 'a':
						case 'b':
						case 'c':
						case 'd':
						case 'v':
						case 'x':
						case 'y':
						case 'z':
							$item = array_merge($item, $subfieldValues);
							break;
					}
				}
				$item = array_map(function($val){ return trim($val,'. '); }, $item);
				$out[] = implode(' / ', $item);
			}
		}
		$out = array_unique($out);
		switch($returnType){
			case 'string' :
				return implode($sep, $out);
			default :
				return $out;
		}
	}

	function getAllEntities(){
		if( ! $this->corporateEntities ) :
			$m = $this->parseMarcRecord();
			$entities = [];
			$names = $this->getValuesFor('110', false);
			if( $names ) :
				$entitiesTypes = $this->getSubfieldValuesFor('110','e',false);
				for($i=0;$i<count($names);$i++){
					$entities[] = array(
						'value' => $names[$i],
						'role' => ($entitiesTypes[$i]) ? $entitiesTypes[$i] : '',
						'primary' => true
					);
				}
			endif;
			$names = $this->getValuesFor('710', false);
			if( $names ) :
				$entitiesTypes = $this->getSubfieldValuesFor('710','e',false);
				for($i=0;$i<count($names);$i++){
					$entities[] = array(
						'value' => $names[$i],
						'role' => ($entitiesTypes[$i]) ? $entitiesTypes[$i] : '',
						'primary' => false
					);
				}
			endif;
			$this->corporateEntities = $entities;
		endif;
		return $this->corporateEntities;
	}

	function getAllPeople(){
		if( ! $this->people ) :
			$m = $this->parseMarcRecord();
			$people = [];
			$names = $this->getValuesFor('100', false);
			if( $names ) :
				$peopleTypes = $this->getSubfieldValuesFor('100','e',false);
				for($i=0;$i<count($names);$i++){
					$people[] = array(
						'value' => $names[$i],
						'role' => ($peopleTypes[$i]) ? $peopleTypes[$i] : 'author',
						'primary' => true
					);
				}
			endif;
			$names = $this->getValuesFor('700', false);
			if( $names ) :
				$peopleTypes = $this->getSubfieldValuesFor('700','e',false);
				for($i=0;$i<count($names);$i++){
					$people[] = array(
						'value' => $names[$i],
						'role' => ($peopleTypes[$i]) ? $peopleTypes[$i] : '',
						'primary' => false
					);
				}
			endif;
			$this->people = $people;
		endif;
		return $this->people;
	}

	function getValuesFor( $code, $single=true, $trimChars = ' ,|/\\') {
		$m = $this->parseMarcRecord();
		if( ! isset($m[$code]) )
			return false;
		$out = array();
		foreach($m[$code] as $un){
			$val = $un['value'];
			if( $trimChars !== false )
				$val = trim($val, $trimChars);
			$val = self::removeMultiSpaces($val);
			$out[] = $val;
		}
		if( $single )
			return $out[0];
		return $out;
	}

	function getSubfieldValuesFor( $code, $subfield, $firstOnly=true, $trimChars = ' ,|/\\') {
		$m = $this->parseMarcRecord();
		if( ! isset($m[$code]) )
			return false;
		$out = [];
		for($i=0;$i<count($m[$code]);$i++){
			if( $firstOnly && $i > 0 )
				break;
			if( ! isset($m[$code][$i]['subfields'][$subfield]) )
				continue;
			$out = array_merge( $out, $m[$code][$i]['subfields'][$subfield]);
		}
		$out = array_unique($out);
		$out = array_map( function($v) use ($trimChars)  { 
			return trim($v,$trimChars);
		}, $out);
		if( $firstOnly )
			return $out[0];
		return $out;
	}

	function getFormattedMarc( $showAllFields = false ){

		$marc = $this->parseMarcRecord();

		$doneSubjects = false;

		$out = '<table class="formatted-marc-display">';
		
		$rows=[];
	
		$people = $this->getAllPeople();
		foreach($people as $person){
			$rows[] = array(
				'label' => ucfirst($person['role']),
				'value' => $person['value']
			);
		}

		foreach( $marc as $code=> $elementsArray ){
			$codename = self::getMarcCodeMapping( $code );

			if( ! $showAllFields && ! $codename ) 
				continue;
			//if there isn't a mapping, just use the code:
			$codename = ($codename===false) ? $code : $codename;


			foreach($marc[$code] as $un){
				// Subjects
				if( strpos($code, '65') === 0){
					$vals = $this->getSubjectsList( $code );

				// Title
				} else if( $code == '245' ) {
					$vals = $this->getTitle();
				
				// Publication Information
				} else if ($code == '264' ) {
					$codename = 'Publication Notice';
					$vals = array(
						$this->getValuesFor('264'),
						$this->getPublisher(),
						$this->getPublicationYear(),
					);
				
				// people
				} else if( $code == '100' || $code == '700') {
					// we've already done these
					continue 2;

				} else {
					// generic
					$vals = $this->getValuesFor($code, false);
				}
			}
			if( is_array($vals) ) {
				$vals = array_unique($vals);
				$vals = implode('<br>', $vals);
			}
			$rows[] = array(
				'label' => $codename,
				'value' => $vals,
			);
		}

		foreach($rows as $row){
			$out .= '<tr>
				<td class="marc-fieldname">'.$row['label'].'</td>
				<td class="marc-fieldvalue">'.$row['value'].'</td>
			';
		}
		
		$out .= '</table>';
		return $out;
	}


	function parseMarcRecord( $rawMarc = null, $forceReparse = false ) {
		if( $this->parsedMarc && $forceReparse === false )
			return $this->parsedMarc;

		$marcString = is_null($rawMarc)
			? $this->rawMarc 
			: $rawMarc;

		if( empty($marcString) ) 
			return false;

		$marcFinal = array();
		$code = '';
		$marcArray = explode("\n",$marcString);

		 // remove leader
		 if( strpos($marcArray[0], "LEADER") === 0 )
			array_shift($marcArray);

		// we'll join multiple lines
		$toUnset = [];
		for($i=0; $i<count($marcArray); $i++){
			$line = $marcArray[$i];
			// if the line starts with 7 spaces, it's a continuation
			if( strpos($line, '       ') === 0 ) {
				$line = substr($marcArray[$i], 6); // leave one of the spaces
				$lastLineStart = $i-1;
				// make sure the previous line wasn't also a multi-line, etc
				while( in_array($lastLineStart, $toUnset) ){
					$lastLineStart--;
				}
				$marcArray[ $lastLineStart ] .= $line;
				// if we unset it right away, will bork the loop
				$toUnset[] = $i;
			}
		}
		foreach($toUnset as $arrayIndex){
			unset($marcArray[$arrayIndex]);
		}

		foreach($marcArray as $line){
			if( empty($line) ) continue;

			preg_match('/(^[0-9]*)([0-9 ]{4})([^\n]*)/',$line,$matches);
			
			// If the 3-digit code is empty, add this line to the 
			// previous line and rely on $code not getting reset 
			// in the loop
			if( empty($matches[1]) ){
				$finalKey = count($marcFinal[$code]) - 1;
				$marcFinal[ $code ][$finalKey]['value'] .= ' ' . trim($matches[3]);
				continue;
			}

			$code = (string) trim($matches[1]);

			if( ! array_key_exists($code, $marcFinal) ){
				$marcFinal[$code] = array();
			}

			$value = trim($matches[3]);
			// parse out the subfields
			$valueParts = explode('|', $value);

			// the first element of the array is the main value
			$newArray = array(
				'value' => array_shift($valueParts),
				'subfields' => [],
			);
			// if there's anything left, it's subfields
			if( count($valueParts) ) {
				foreach($valueParts as $subfield){
					$subfieldKey = strval( substr($subfield,0,1) );
					$subfieldValue = trim( substr($subfield,1) );
					// if it's already set, make it an array of values. We could make it an array from the
					// beginning, but it'd be harder to work with 90% of the time for
					// the sake of knowing it's always an array.
					/* * /
					if( isset($newArray['subfields'][$subfieldKey]) ) {
						if( ! is_array($newArray['subfields'][$subfieldKey]) ) {
							$newArray['subfields'][$subfieldKey] = array(
								$newArray['subfields'][$subfieldKey]
							);
						}
						$newArray['subfields'][ $subfieldKey ][] = $subfieldValue;
					} else {
						$newArray['subfields'][ $subfieldKey ] = $subfieldValue;
					}
					/**/
					// alternatively, always make them arrays
					/**/
					if( ! isset($newArray['subfields'][$subfieldKey]) )
						$newArray['subfields'][$subfieldKey] = array();
					$newArray['subfields'][$subfieldKey][] = $subfieldValue;
					/**/

				}
			}

			$matches[2] = trim($matches[2]);
			if( ! empty($matches[2]) )
				$newArray['indicators'] = $matches[2];

			$marcFinal[$code][] = $newArray;
		}

		$this->parsedMarc = $marcFinal;
		return $this->parsedMarc;
	}


	static function getMarcCodeMapping( $code=null ){
		$map = array(
			"020" => 'ISBNs',
			"100" => 'Author',
			"110" => 'Corporate Author',
			"245" => 'Title',
			"246" => 'Alternate Title',
			"250" => 'Edition',
			"260" => 'Published',
			"264" => 'Published',
			"300" => 'Physical Description',
			"490" => 'Series',
			"500" => 'Note',
			"520" => 'Summary',
			"650" => 'Subject - Topical',
			"651" => 'Subject - Geographical',
			"655" => 'Genre/Form',
			"700" => 'Additional Individuals',
			"710" => 'Additional Corporate Entities',
		);
		if( is_null($code) )
			return $map;
		if( ! isset($map[$code]) )
			return false;
		return $map[$code];
	}

	/**
	 * Reduce all multiple spaces down to one
	 */
	static function removeMultiSpaces( $str ){
		if( ! is_string($str) )
			return $str;
		while( strpos($str, '  ') !== false )
			$str = str_replace('  ', ' ', $str);
		return $str;
	}
	

}